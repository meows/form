import { createStore } from 'redux'
import ReactDOM        from 'react-dom'
import React           from 'react'

// -----------------------------------------------------------------------------
// Component

const render = function() {
   ReactDOM.render(
      <BookmarkForm
         value={store.getState()}
      />,
      document.getElementById('app')
   )
}

const BookmarkForm = function({ value }) {
   function saveForm() {
      store.dispatch({
         type: 'NEW_BOOKMARK',
         data: addBookmark(jsonConverter()),
      })
   }
   function resetForm() {
      store.dispatch({ type: 'RESET' })
   }

   return (
      <section>
         <h4>{value}</h4>
         <form name='bookmarkForm' id='form'>

            <div id='row'>
               <label>URL</label>
               <input name='url' type='text' />
            </div>

            <div id='row'>
               <label>Title</label>
               <input name='title' type='text' />
            </div>

            <div id='row'>
               <label>About</label>
               <textarea rows='4' name='about'></textarea>
            </div>

            <div id='row'>
               <label>Tags</label>
               <input name='tags' type='text' />
            </div>

            <div id='row'>
               <div className='cell'></div>
               <button onClick={saveForm} type='button' className='ui button'>Submit</button>
               <button onClick={resetForm} type='button' className='ui button'>Cancel</button>
            </div>
         </form>
      </section>
   )
}

// -----------------------------------------------------------------------------
// Store

const bookmarkReducer = function(state = 'Initial State', action) {
   switch (action.type) {
      case 'NEW_BOOKMARK':
         return JSON.stringify(action.data)
      case 'RESET':
         return 'State Reset'
      default:
         return state
      ;
   }
}

const store = createStore(bookmarkReducer)
store.subscribe(render)
render()

// -----------------------------------------------------------------------------
// Network

function poster(payload) {
   var url = 'https://httpbin.org/post';
   var options = {
      method : 'POST',
      mode   : 'cors',
      body   : payload,
   }
   return fetch(url, options)
      .then(x => x.json())
      .then(x => console.log(x))
      .catch(err => console.log(err))
   ;
}

const networkReducer = function(state = 'IDLE', action) {
   switch (action.type) {
      case 'IDLE':
         return 'IDLE'
      case 'PENDING':
         return 'PENDING'
      case 'FAILURE':
         return 'FAILURE'
      default:
         return state
      ;
   }
}

// -----------------------------------------------------------------------------
// Add Bookmark

// () => form => FormData => Object
function jsonConverter() {
   var complexObject = new FormData(document.querySelector('form'))
   var result = {}
   for (var pair of complexObject.entries()) { result[pair[0]] = pair[1] }
   result.tags = result.tags
      .trim()
      .replace(/\s+/g,' ')
      .split(' ')
   ;

   return result
}

function addBookmark(rawData, rawTime = new Date()) {
   return {
      type  : 'ADD_BOOKMARK',
      data  : {
         id    : rawTime.getTime(),
         url   : rawData.url,
         title : rawData.title,
         about : rawData.about,
         tags  : rawData.tags,
         time  : {
            created: {
               year    : rawTime.getFullYear(),
               month   : rawTime.getMonth(),
               day     : rawTime.getDate(),
               hour    : rawTime.getHours(),
               minutes : rawTime.getMinutes(),
            },
            updated: {
               year    : null,
               month   : null,
               day     : null,
               hour    : null,
               minutes : null,
            },
         },
      },
   };
}

async function meow() {
   var a = await fetch(url)
      .then(x => x.json())
      .then(x => a = x)

   return a
}
console.log(meow())

const url = "https://api.github.com/repos/vmg/redcarpet/issues?state=closed"
