var path    = require('path');
var webpack = require('webpack');

module.exports = {
   entry: ['babel-polyfill', './main.jsx'],
   output: { path: __dirname, filename: 'bundle.js' },
   module: {
      loaders: [
         {
            test: /\.jsx?$/,
            loader: 'babel-loader',
            exclude: /node_modules/,
            query: {
               plugins: [ 'transform-async-to-generator' ],
               presets: [ 'stage-0', 'es2015', 'react'   ],
            },
         },
      ],
   },
};
