## Installation assumes you have:

 * git
 * node + npm
 * an OSX or Linux-like command line environment

## Installation instructions:

1. Clone repository to a folder of your choice: `git clone git@bitbucket.org:meows/form.git`.
2. Navigate to the newly cloned repository and type `npm install` to install everything in `package.json` locally.
3. Install webpack-dev-server globally with `npm install webpack-dev-server -g`.
4. Type `webpack-dev-server` in your shell and you should find a message about compilation progress. Upon successful completion, it'll notify you that your app is being hosted at `http://localhost:8080/webpack-dev-server/`.

